import React from "react";
import '../css/thumbs.css';

export default class Thumbs extends React.Component{
    
    constructor(props){
        super(props);
        this.state = {
            on : this.props.estadoInicial
        }
        this.pulsar = this.pulsar.bind(this);
    }


    pulsar(){
        this.setState({
            on: !this.state.on
        })
    }
    
    render(){
        let claseIcono = (this.state.on) ? "fas fa-thumbs-up" : "fas fa-thumbs-down";
        let claseDiv = (this.state.on) ? "thumbs on" : "thumbs off" ;
    return (
        <div onClick={this.pulsar} className={claseDiv}>
            <i className={claseIcono} />
            <span>{this.props.texto}</span>
         </div>
    );
    }
}