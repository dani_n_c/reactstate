import React from 'react';

export default class Previsio extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            list: []
        };

        this.getPrevisio = this.getPrevisio.bind(this);
        this.getPrevisio();
    }

    getPrevisio(){
        const ciutat = "barcelona,es";
        const apiKey = "84dbcf8c3480649bce9d4bb58da44b4e";
        const funcio = "forecast";
        const apiUrl = `http://api.openweathermap.org/data/2.5/${funcio}?q=${ciutat}&APPID=${apiKey}&units=metric`;

        fetch(apiUrl)
            .then(response => response.json())
            .then(data => this.setState( data ))
            .catch(error => console.log(error));
    }

    render() {

        if (!this.state.list.length) {
            return <h1>Cargando datos...</h1>
        }
        let fechaHoy= new Date (this.state.list[0].dt*1000);
        let fecha = new Date(this.state.list[0].dt * 1000);
        let fecha2 = new Date(this.state.list[1].dt * 1000);
        
        let fechaMas1= new Date (fechaHoy.setDate(fecha.getDate() + 1));
        let fechaMas2= new Date (fechaHoy.setDate(fecha.getDate() + 2));
        let fechaMas3= new Date (fechaHoy.setDate(fecha.getDate() + 3));
        let fechaMas4= new Date (fechaHoy.setDate(fecha.getDate() + 4));
        
        
        for(let i=0; i<this.state.list.length;i++){
           let dato= this.state.list[i].dt;
            if (dato==fecha/1000){
                console.log("Fecha hoy");
                console.log(dato);
                
            }
            if(dato == fechaMas1/1000 ){
                console.log("Dia mas 1");
                console.log(dato);
            }
           if(dato ==fechaMas2/1000 ){
               console.log("Dia mas 2")
               console.log(dato);
           }
           if(dato ==fechaMas3/1000 ){
            console.log("Dia mas 3")
            console.log(dato);
        }
        if(dato ==fechaMas4/1000 ){
            console.log("Dia mas 4")
            console.log(dato);
        }
 
        }
        
        
        
        
        return (
            <>
            <h3>Previsión para {fecha.toString()}</h3>
            <h3>Temperatura {this.state.list[0].main.temp}</h3>
            <h3>{this.state.list[0].dt_txt}</h3>
            
            <h3>Previsión para {fecha2.toString()}</h3>
            <h3>Temperatura {this.state.list[1].main.temp}</h3>
     
            </>
        ); 
    }
}
