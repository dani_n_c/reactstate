import React from "react";
import '../css/tricolor.css';


class Tricolor extends React.Component{
    constructor(props){
        super(props);
       
         this.state = { color: 0};            
        
        this.clicar = this.clicar.bind(this);
    }
    clicar(){
       let nuevoColor = 0;
       if (this.state.color<3){
           this.setState({color: this.state.color+1});
       }else{
        this.setState({color: 0});
       }
       
    }
    render (){
        let clases = ["ciculo", "circulo rojo", "circulo verde", "circulo azul"];
        let bColor = clases[this.state.color];
        

    return (
        <div onclick={this.clicar} className={bColor}>
        </div>
    );
}
}